#include <MyRealTimeClock.h>                                                                          
MyRealTimeClock myRTC(D1, D2, D3); // Pin assigned 

void setup()  {      
  Serial.begin(9600);

  /* To set the current time and date in specific format 
| Second 00 | Minute 59 | Hour 10 | Day 12 |  Month 07 | Year 2015 |
*/
//  myRTC.setDS1302Time(00, 36, 18, 6, 26, 12, 2019);
}


void loop()  {                                                                                            
                                                                           
  myRTC.updateTime();                                                                                     
  Serial.print(myRTC.hours);
  Serial.print(":");
  Serial.print(myRTC.minutes);
  Serial.print(":");
  Serial.println(myRTC.seconds);
  }                                                                                                         
                                                                                                             
